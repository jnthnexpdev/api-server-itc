import mongoose from "mongoose";
const schema = mongoose.Schema;

const teacherListModel = new schema({
    Nombre_Docente : {type : String, required : true},
    Fecha_Registro : {type : String, required : true}
});

const teachers = mongoose.model('Docentes_Registrados', teacherListModel);

export default teachers;