import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const schema = mongoose.Schema;

const teacherModel = new schema({
    Nombre : {type : String, required : true},
    Correo : {type : String, required : true},
    Cuenta : {type : String, required : true},
    Fecha_Registro : {type : String},
    Hora_Registro : {type : String},
    Password : {type : String, required : true},
    Grupos : {
        Clave_Grupo : {type: String, required : true},
    }
});

teacherModel.pre('save', async function(next) {
    try{
        const salt = 12;
        const hash = await bcrypt.hash(this.Password, salt);
        this.Password = hash;
        next();
    }catch(error){
        next(error);
    }
});

const teacher = mongoose.model('Docentes', teacherModel);

export default teacher;