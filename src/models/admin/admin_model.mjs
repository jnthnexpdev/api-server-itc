import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const schema = mongoose.Schema;

const adminModel = new schema({
    Nombre : {type : String, required : true},
    Correo : {type : String, required : true},
    Cuenta : {type : String, required : true},
    Departamento : {type : String, required : true},
    Fecha_Registro : {type : String},
    Hora_Registro : {type : String},
    Password : {type : String, required : true},
});

adminModel.pre('save', async function(next) {
    try{
        const salt = 12;
        const hash = await bcrypt.hash(this.Password, salt);
        this.Password = hash;
        next();
    }catch(error){
        next(error);
    }
});

const admin = mongoose.model('Administradores', adminModel);

export default admin;