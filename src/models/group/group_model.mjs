import mongoose from "mongoose";
const schema = mongoose.Schema;

const groupModel = new schema({
    Semestre : {type : Number, required : true},
    Materia : {type : String, required : true},
    Docente : {type : mongoose.Schema.Types.ObjectId, ref : 'Docentes'},
    Periodo : {type : String},
    Numero_Grupo : {type : Number, required : true}
});

const group = mongoose.model('Grupos', groupModel);

export default group;