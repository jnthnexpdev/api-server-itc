import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const studentsModel = new Schema({
    Alumno : [{
        Nombre_Alumno : {type : String, required : true},
        Numero_Control : {type : String, required : true},
        Calificaciones_Alumno : [{
            Unidad : [{
                Actividades : [{
                    Numero_Actividad : {type : Number},
                    Calificacion_Actividad : {type : Number}
                }],
                Promedio_Unidad : {type : Number}
            }]
        }],
        Promedio_Final : {type : Number}
    }]
});

const student = mongoose.model('Alumnos', studentsModel);

export default student;