import mongoose, { Types } from 'mongoose';

const schema = mongoose.Schema;

const signatureModel = new schema({
   Clave_Materia : {type : String, required : true},
   Nombre_Materia : {type : String, required : true},
   Unidades : [{
      Unidad : {type : Number, required : true},
      Actividades : [{
         Actividad : {type : String, required : true},
         Porcentaje : {type : Number, required : true}
      }]
   }],
   Alumnos_Aprobados : {type : Number},
   Alumnos_Reprobados : {type : Number},
   Alumnos_Desertados : {type : Number},
   Promedio_General: {type : Number},
   Lista_Alumnos : {type : mongoose.Schema.Types.ObjectId, ref : 'Alumnos'}
});

const signature = mongoose.model('Materias', signatureModel);

export default signature;