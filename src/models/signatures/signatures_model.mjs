import mongoose from 'mongoose';
const schema = mongoose.Schema;

const signaturesModel = new schema({
    Materia : {type : String, required : true},
    Clave_Materia : {type : String, required : true},
    Semestre_Materia : {type : Number, required : true},
    Departamento : {type : String, required : true}
});

const signatures = mongoose.model('Kardex', signaturesModel);

export default signatures;