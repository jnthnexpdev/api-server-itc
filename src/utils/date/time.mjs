import axios from 'axios';
import moment from 'moment';

export const getTimeAPI = async () => {
    try{
        const response = await axios.get('http://worldtimeapi.org/api/timezone/America/Mexico_City');
        const {date} = response.data;
        const formattedTime = moment(date).format('h:mm A');
        
        return formattedTime;
    }catch(error){
        console.error('Error at get date');
        throw error;
    }
}