import axios from 'axios';
import moment from 'moment';

export const getDateAPI = async () => {
    try{
        const response = await axios.get('http://worldtimeapi.org/api/timezone/America/Mexico_City');
        const {date} = response.data;
        const formattedDate = moment(date).format('DD-MM-YYYY');
        
        return formattedDate;
    }catch(error){
        console.error('Error at get date');
        throw error;
    }
}