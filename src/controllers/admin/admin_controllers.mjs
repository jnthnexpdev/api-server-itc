import adminModel from '../../models/admin/admin_model.mjs';
import { getDateAPI } from '../../utils/date/date.mjs';
import { getTimeAPI } from '../../utils/date/time.mjs';

export const createAdminAccount = async(req, res) => {
    try{
        const body = req.body;
        const Fecha  = await getDateAPI();
        const Hora = await getTimeAPI();

        const existingAdmin = await adminModel.findOne({Correo : body.Correo});

        if(existingAdmin){
            return res.status(400).json({
                success : false,
                message : 'Correo ya registrado'
            });
        }

        const admin = new adminModel({
            Nombre : body.Nombre,
            Correo : body.Correo,
            Cuenta : 'Administrador',
            Fecha_Registro : Fecha,
            Hora_Registro : Hora,
            Password : body.Password,
        });

        await admin.save(); 

        return res.status(200).json({
            success : true,
            message : 'Administrador registrado',
        });

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success : false,
            message : 'Error del servidor'
        });
    }
} 

export const listAdmins = async(req, res) => {
    
    try{
        const admins = await adminModel.find().select('-Password');

        if(admins.length === 0 || admins === null){
            return res.status(400).json({
                success : false,
                message : 'No hay administradores'
            });
        }

        return res.status(200).json({
            success : true,
            admins : admins
        });

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success : false,
            message : 'Error del servidor'
        });
    }

}