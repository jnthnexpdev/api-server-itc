import teacherModel from '../../models/teacher/teacher_model.mjs';
import { getDateAPI } from '../../utils/date/date.mjs';
import { getTimeAPI } from '../../utils/date/time.mjs';

export const createTeacherAccount = async (req, res) => {
    try{
        const body = req.body;
        const Fecha  = await getDateAPI();
        const Hora = await getTimeAPI();

        const existingTeacher = await teacherModel.findOne({Correo : body.Correo});

        if(existingTeacher){
            return res.status(400).json({
                success : false,
                message : 'Correo ya registrado'
            });
        }

        const teacher = new teacherModel({
            Nombre : body.Nombre,
            Correo : body.Correo,
            Cuenta : 'Docente',
            FechaRegistro : Fecha,
            HoraRegistro : Hora,
            Password : body.Password,
        });

        await teacher.save();

        return res.status(200).json({
            success : true,
            message : 'Docente registrado'
        });

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success : false,
            message : 'Error del servidor'
        });
    }
}

export const listTeachers = async(req, res) => {
    try{
        const teachers = await teacherModel.find().select('-Password');

        if(teachers.length === 0 || teachers === null){
            return res.status(400).json({
                success : false,
                message : 'No hay administradores'
            });
        }

        return res.status(200).json({
            success : true,
            teachers : teachers
        });

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success : false,
            message : 'Error del servidor'
        });
    }
}