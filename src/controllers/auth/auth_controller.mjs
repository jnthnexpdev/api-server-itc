import teacherModel from '../../models/teacher/teacher_model.mjs';
import adminModel from '../../models/admin/admin_model.mjs';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

import authToken from '../../middleware/auth_token.mjs';
import { getDateAPI } from '../../utils/date/date.mjs';
import { getTimeAPI } from '../../utils/date/time.mjs';

export const loginUser = async(req, res) => {
    try{
        const {Correo, Password} = req.body;
        const userData = await getDataUser(Correo);

        if(!userData){
            return res.status(400).json({
                success : false,
                message : 'No hay una cuenta registrada con este correo'
            });
        }

        if(bcrypt.compareSync(Password, userData.Password)){
            const { token } = await createToken(userData);

            res.cookie('dataSession', token, {
                httpOnly : false,
                signed : true,
            });

            return res.status(200).json({
                success : true,
                message : 'Sesion iniciada',
                token : token
            });
        }else{
            return res.status(401).json({
                success : false,
                message : 'Datos incorrectos'
            });
        }

    }catch(error){
        console.log(error);
        return res.status(500).json({
            success : false,
            message : 'Error del servidor'
        });
    }
}

export const authUser = async(req, res) => {
    try{
        await authToken()(req, res, async(error) => {
            if(error){
                return res.status(401).json({
                    success : false,
                    message : 'Usuario no autenticado'
                }); 
            }
            
            return res.status(200).json({
                success : true,
                message : 'Usuario autenticado'
            });
        });
    }catch(error){
        return res.status(500).json({
            success : false,
            message : 'Error interno del servidor'
        });
    }
}

export const typeUser = async(req, res) => {
    try{
        const cookie = req.signedCookies.dataSession;

        if(!cookie){
            return res.status(401).json({
                success : false,
                message : 'No se ha proporcionado la cookie'
            });
        }

        const decoded = await jwt.verify(cookie, process.env.SECRET);
        const user = decoded.user;

        return res.status(200).json({
            account : user
        });

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success : false,
            message : 'Error interno del servidor'
        });
    }
}

export const searchUser = async(req, res) => {
    try{
        const cookie = req.signedCookies.dataSession;

        const decoded = await jwt.verify(cookie, process.env.SECRET);
        const id = decoded._id;

        const user = await getDataUserById(id);

        if(!user){
            return res.status(400).json({
                success : false,
                message : 'Usuario no encontrado'
            });
        }

        return res.status(200).json({
            success : true,
            user : user
        });
        
    }catch(error){
        console.log(error);
        return res.status(500).json({
            success : false,
            message : 'Error del servidor'
        });
    }
}

async function getDataUser(Correo){
    try{
        const admin = await adminModel.findOne({ Correo : Correo});
        const teacher = await teacherModel.findOne({Correo : Correo});
        return admin || teacher;
    }catch(error){
        console.error(error);
        throw new Error('Error al buscar el usuario');
    }
}

async function getDataUserById(Id){
    try{
        const admin = await adminModel.findById(Id).select('-Password');
        const teacher = await teacherModel.findById(Id).select('-Password');
        return admin || teacher;
    }catch(error){
        console.error(error);
        throw new Error('Error al buscar el usuario');
    }
}

async function createToken(User){
    try{
        const Hora = await getTimeAPI();
        const Fecha = await getDateAPI();

        const tokenData = {
            _id : User._id,
            user : User.Cuenta,
            name : User.Nombre,
            lastname : User.Apellidos,
            time : Hora,
            date : Fecha
        };

        const token = jwt.sign(
            tokenData, 
            process.env.SECRET,
            { expiresIn : '1h' },
            { algorithm: 'HS256' }
        );
        return { token };
    }catch(error){
        console.error(error);
        throw new Error('Error al crear el token');
    }
}