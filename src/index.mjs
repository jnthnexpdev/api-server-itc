import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import router from './routes/index_routes.mjs';
import {connectDatabase} from './config/db.mjs';
import dotenv from 'dotenv';
dotenv.config();

const app = express();
const PORT = process.env.PORT || 3000;

//Json config
app.use(bodyParser.json({limit : '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended : true}));
app.use(bodyParser.json());

//Cors, helmet and headers
app.use( cors( {origin : 'http://localhost:4200', credentials : true}));
// app.use(helmet());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE"); // Corregir aquí
    next();
});

connectDatabase();

//Router of routes api
app.use(router);

//Listen port
app.listen(PORT, async () => {
    console.log(`API Server on port ${PORT}`);
});

export default app;