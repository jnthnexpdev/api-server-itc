import express from 'express';
import cookieParser from 'cookie-parser';
import adminToken from '../../middleware/admin_token.mjs';
import * as teacherControllers from '../../controllers/teacher/teacher_controllers.mjs';
import dotenv from 'dotenv';
dotenv.config();

const token_key = process.env.SECRET;
const router = express.Router();

router.use(cookieParser(token_key));

router.post('/api/v1/itc/teacher/create-account', adminToken(), teacherControllers.createTeacherAccount);
router.get('/api/v1/itc/teacher/list', adminToken(), teacherControllers.listTeachers);

export default router;