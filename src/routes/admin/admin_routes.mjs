import express from 'express';
import cookieParser from 'cookie-parser';
import adminToken from '../../middleware/admin_token.mjs';
import * as adminControllers from '../../controllers/admin/admin_controllers.mjs';
import dotenv from 'dotenv';
dotenv.config();

const token_key = process.env.SECRET;
const router = express.Router();

router.use(cookieParser(token_key));

router.post('/api/v1/itc/admin/create-account', adminToken(), adminControllers.createAdminAccount);
router.get('/api/v1/itc/admin/list', adminToken(), adminControllers.listAdmins);

export default router;