import express from 'express';
import cookieParser from 'cookie-parser';
import authToken from '../../middleware/auth_token.mjs';
import * as authController from '../../controllers/auth/auth_controller.mjs';
import dotenv from 'dotenv';
dotenv.config();

const token_key = process.env.SECRET;
const router = express.Router();

router.use(cookieParser(token_key));

router.post('/api/v1/itc/auth/login', authController.loginUser);
router.get('/api/v1/itc/auth/user-info', authToken(), authController.searchUser);
router.get('/api/v1/itc/auth/user-logged', authController.authUser);
router.get('/api/v1/itc/auth/user-type', authToken(), authController.typeUser);

export default router;