import express from 'express';
import teacherRoutes from '../routes/teacher/teacher_routes.mjs';
import adminRoutes from '../routes/admin/admin_routes.mjs';
import authRoutes from '../routes/auth/auth_routes.mjs';
const router = express.Router();

router.use(adminRoutes);
router.use(teacherRoutes);
router.use(authRoutes);

export default router;